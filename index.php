<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "blog";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SET CHARACTER SET UTF8";
$result = $conn->query($sql);

$sql = "SELECT COUNT(id) as count FROM articles";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $idCount = $row["count"];
} else {
    echo "0 results";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Výpis článků</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="wrap">
        <h2>Seznam článků</h2>
        <?php

        for ($id = 1; $id <= $idCount; $id++) {

            $sql = "SELECT * FROM articles WHERE id='$id'";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {

                while ($row = $result->fetch_assoc()) {
                    $nameArticle = $row["nameArticles"];
                    $author = $row["author"];
                    $category = $row["category"];
                    $datePublic = $row["datePublic"];
                    $img = $row["img"];

                    echo "<form action='getArticle.php' method='get' class='article'>
                    <input type='hidden' name='id' value='$id'>

                    <p class='__img'><img src='$img' alt='article image'></p>
                    
                    <div id='right'><input type='submit' value='$nameArticle'>
                    <div id='informace'><p class='author'>Autor: $author</p>
                    <p class='category'>Kategorie: $category</p></div></div>
                    </form><br><hr><br>";
                }
            } else {
                echo "0 results";
            }
        }

        ?>
    </div>
</body>
</html>

<?php
$conn->close();
?>
